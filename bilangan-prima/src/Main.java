import java.util.*;

public class Main {
    public static void main(String[] args) {
        Integer number;
        Boolean result = false;

        Scanner keyboard = new Scanner(System.in);
        System.out.print("Masukkan angka: ");
        number = keyboard.nextInt();

        int i = 2;

        if (number < 2) {
            System.out.println("Bukan Bilangan Prima");
        }
        while (i < number) {
            if (number == 2 || number == 3) {
                result = true;
                break;
            } else if (number %i == 0) {
                result = false;
                break;
            } else {
                result = true;
            }
            i++;
        }
        if (result) {
            System.out.println("Bilangan Prima");
        } else {
            System.out.println("Bukan Bilangan Prima");
        }
    }
}